// ==UserScript==
// @name        Vimeo Videos Button
// @description Add a link from a user page to the videos of that user
// @copyright 2020, marcowahl (https://openuserjs.org/users/marcowahl)
// @licence GPL-3.0-or-later; http://www.gnu.org/licenses/gpl-3.0.txt
// @include     https://vimeo.com/*
// @version     0.0.1
// @grant       none
// @author   Marco Wahl
// @homepageURL https://gitlab.com/marcowahl/vimeo-videos-button
// @icon https://vimeo.com/favicon.ico
// ==/UserScript==

'use strict'

let button = document.createElement('a')
button.href = window.location.href + "/" + "videos"
button.innerHTML = " | VIDEOS"
button.style.color = '#f22'
button.onmouseenter = function () {
  button.style.color = 'rgb(68,187,255)'
}
button.onmouseleave = function () {
  button.style.color = '#aaa'
}
let subheadline = document.querySelector("h2.l-text-center.profile_subheadline__text")
if (subheadline) subheadline.appendChild(button)
